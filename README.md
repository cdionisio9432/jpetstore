Monolito Pipeline Demo
=================

Aplicación maven que genera un artefacto WAR y realiza despliegue automático a un JBoss

### Jenkins Pipeline [link-pipeline](http://207.154.193.52:8080/job/monolito-pipeline/)

![jenkins-pipeline](./docs/pipeline.png)

### Cobertura SonarQube [link-sonar](http://207.154.193.52:9000/dashboard?id=org.mybatis%3Ajpetstore)

![cobertura-sonar](./docs/sonarqube.png)

### Nexus Artifactory [link-nexus](http://207.154.193.52:8083/#browse/browse:jpetstore)

![nexus](./docs/nexus-arrtifactory.png)

### JBoss Admin Deployments [link-jboss](http://147.182.135.249:9990/console/index.html#home)

![jboss](./docs/jboss-deployment.png)

### Applicación Desplegada :') [link](http://147.182.135.249:8080/jpetstore-6.0.3/)

![jboss](./docs/aplicacion.png)
